FROM i386/ubuntu:trusty


LABEL org.label-schema.build-date=2019-11-21 \
  maintainer="dtulyakov@gmil.com" \
  org.label-schema.description="docker run --detach --tty --env=\"DISPLAY\" --name=lotus -v/home/user/.Xauthority:/home/indocker/.Xauthority -v/home/user/lotus:/home/indocker/lotus" \
  org.label-schema.name="lotus" \
  org.label-schema.url="https://hub.docker.com/repository/docker/dtulyakov/lotus" \
  org.label-schema.vcs-url="https://bitbucket.org/dtulyakov/docker-lotus" \
  org.label-schema.vendor="Denis Tulyakov" \
  org.label-schema.version=1.0


RUN set -x \
  && apt-get update -q \
  && apt-get install -y \
     default-jre \
     libxt6 \
     libpangoxft-1.0-0 \
     libart-2.0-2 \
     libbonoboui2-0 \
     libgnome-keyring0 \
     libpangox-1.0-0 \
     libxss1 \
     libgnomeui-0 \
     xterm \
  && apt-get clean autoclean \
  && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*

ADD lib/* /usr/lib/i386-linux-gnu/
ADD deb /tmp/deb

RUN set -x\
  && dpkg -i \
       /tmp/deb/ibm-notes-core-ru-9.0.i586.deb \
       /tmp/deb/ibm-notes-nl2-9.0.i586.deb \
       /tmp/deb/ibm-notes-nodep-9.0.i586.deb \
  && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*


CMD ["/opt/ibm/notes/notes"]
