[![Docker Pulls](https://img.shields.io/docker/pulls/dtulyakov/lotus.svg)][hub]

[![](https://images.microbadger.com/badges/version/dtulyakov/lotus:latest.svg)](https://microbadger.com/images/dtulyakov/lotus:latest "latest")[![](https://images.microbadger.com/badges/image/dtulyakov/lotus:latest.svg)](https://microbadger.com/images/dtulyakov/lotus:latest "latest")

[hub]: https://hub.docker.com/r/dtulyakov/lotus/


Create directory

```shell
mkdir -p ~/lotus/notes/data
```
Put your file `user.id` in the directory `~/lotus/notes/data`

Run lotus `lotus-run.sh`

[Download lotus-run.sh](https://bitbucket.org/dtulyakov/docker-lotus/src/master/lotus-run.sh)
