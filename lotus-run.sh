#!/bin/bash

IMAGE_NAME=dtulyakov/lotus
CONTAINER_ID=lotus

TEST_CONTAINER_ID=$(docker ps -qa --no-trunc --filter name=^/${CONTAINER_ID}$)
[ "${TEST_CONTAINER_ID}" == "" ] && {
  docker run \
      --tty \
      --detach \
      --restart=no \
      --net=host \
      --env="DISPLAY" \
      --name=${CONTAINER_ID} \
      -v${HOME}/.Xauthority:/root/.Xauthority \
      -v${HOME}/lotus:${HOME}/lotus \
      $IMAGE_NAME bash;
  docker exec $CONTAINER_ID bash -c "useradd ${USER} && chown ${USER}.${USER} ${HOME}";
    }
docker restart ${CONTAINER_ID}
docker exec -u ${USER} ${CONTAINER_ID} /opt/ibm/notes/notes
